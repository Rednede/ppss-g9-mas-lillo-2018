/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3;

/**
 *
 * @author ppss
 */
public class ServiceFactory {
    private static IOperacionBO operacion = null;
    
    public static IOperacionBO create(){
        if(operacion != null){
            return operacion;
        }else{
            return new Operacion();
        }
    }
    
    public static void setOperacion (IOperacionBO o){
        operacion = o;
    }
}
