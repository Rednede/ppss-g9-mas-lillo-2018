/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3.testsUnitarios;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ppss.ejercicio3.OperacionStub;
import ppss.ejercicio3.ReservaTestable;
import ppss.ejercicio3.ServiceFactory;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class ReservaTest {
    
    public ReservaTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void realizaReservaTestC1() {
        String expected = "ERROR de permisos; "; 
        
        ReservaTestable sut = new ReservaTestable();
        OperacionStub OStub = new OperacionStub();
        OStub.setErrorType(expected);
        ServiceFactory.setOperacion(OStub);
        sut.setPermission(false);
        String login = "xxxx";
        String password = "xxxx";
        String socio = "Luis";
        String[] isbns = {"11111"};

        try {
            sut.realizaReserva(login, password, socio, isbns);
            fail("adasdasd2");
        }catch(Exception e){
            assertEquals(expected, e.getMessage());
        }
        
    }
    
    @Test
    public void realizaReservaTestC2() {
        String expected = ""; 
        
        ReservaTestable sut = new ReservaTestable();
        OperacionStub OStub = new OperacionStub();
        OStub.setErrorType("");
        ServiceFactory.setOperacion(OStub);
        sut.setPermission(true);
        String login = "ppss";
        String password = "ppss";
        String socio = "Luis";
        String[] isbns = {"11111","222222"};

        try {
            sut.realizaReserva(login, password, socio, isbns);

        }catch(Exception e){
            assertEquals(expected, e.getMessage());
        }
        
    }
    
    @Test
    public void realizaReservaTestC3() {
        String expected = "ISBN invalido:33333; "; 
        
        ReservaTestable sut = new ReservaTestable();
        OperacionStub OStub = new OperacionStub();
        OStub.setErrorType("IsbnInvalidoException");
        ServiceFactory.setOperacion(OStub);
        sut.setPermission(true);
        String login = "ppss";
        String password = "ppss";
        String socio = "Luis";
        String[] isbns = {"33333"};

        try {
            sut.realizaReserva(login, password, socio, isbns);
            fail("Fallo de ISBN");
        }catch(Exception e){
            assertEquals(expected, e.getMessage());
        }
        
    }
    
    @Test
    public void realizaReservaTestC4() {
        String expected = "SOCIO invalido; "; 
        
        ReservaTestable sut = new ReservaTestable();
        OperacionStub OStub = new OperacionStub();
        OStub.setErrorType("SocioInvalidoException");
        ServiceFactory.setOperacion(OStub);
        sut.setPermission(true);
        String login = "ppss";
        String password = "ppss";
        String socio = "Pepe";
        String[] isbns = {"11111"};

        try {
            sut.realizaReserva(login, password, socio, isbns);
            fail("adasdasd2");
        }catch(Exception e){
            assertEquals(expected, e.getMessage());
        }
        
    }
    
    @Test
    public void realizaReservaTestC5() {
        String expected = "CONEXION invalida; "; 
        
        ReservaTestable sut = new ReservaTestable();
        OperacionStub OStub = new OperacionStub();
        OStub.setErrorType("JDBCException");
        ServiceFactory.setOperacion(OStub);
        sut.setPermission(true);
        String login = "ppss";
        String password = "ppss";
        String socio = "Luis";
        String[] isbns = {"11111"};

        try {
            sut.realizaReserva(login, password, socio, isbns);
            fail("adasdasd2");
        }catch(Exception e){
            assertEquals(expected, e.getMessage());
        }
        
    }
    
    
}
