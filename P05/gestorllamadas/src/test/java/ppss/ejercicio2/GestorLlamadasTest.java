/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio2;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class GestorLlamadasTest {
    
    public GestorLlamadasTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void CalculaConsumoTestC1() {
        GestorLlamadas sut = new GestorLlamadas();
        CalendarioStub c = new CalendarioStub();
        c.setHoraActual(15);
        sut.setCalendario(c);
        double resultadoEsperado = 208;
        double resultadoReal = sut.calculaConsumo(10);
        assertEquals(resultadoEsperado, resultadoReal, 0.001);
    }
    
    @Test
    public void CalculaConsumoTestC2() {
        GestorLlamadas sut = new GestorLlamadas();
        CalendarioStub c = new CalendarioStub();
        c.setHoraActual(22);
        sut.setCalendario(c);
        double resultadoEsperado = 105;
        double resultadoReal = sut.calculaConsumo(10);
        assertEquals(resultadoEsperado, resultadoReal, 0.001);
    }
}
