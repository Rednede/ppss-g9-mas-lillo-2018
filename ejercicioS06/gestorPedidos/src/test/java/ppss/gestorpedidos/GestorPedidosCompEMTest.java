/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.gestorpedidos;

import org.easymock.EasyMock;
import static org.easymock.EasyMock.anyObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;
import ppss.gestorpedidos.Buscador;
import ppss.gestorpedidos.Cliente;
import ppss.gestorpedidos.Factura;
import ppss.gestorpedidos.FacturaException;
import ppss.gestorpedidos.GestorPedidos;
import ppss.gestorpedidos.interfaces.ComportamientoTest;


/**
 *
 * @author ppss
 */
@Category (ComportamientoTest.class)
public class GestorPedidosCompEMTest {
    GestorPedidos sut;
    Buscador mock;
    Cliente cli;
    
    @Before
    public void setUp() {
        sut = new GestorPedidos();
    }
    @Test
    public void generarFacturaTestCompC1() {
        cli = new Cliente("cliente1",20.0f);
        mock = EasyMock.createMock(Buscador.class);
        EasyMock.expect(mock.elemPendientes(cli)).andReturn(10);
        sut.setBuscador(mock);
        EasyMock.replay(mock);
        

        Factura resultadoEsperado = new Factura("cliente1");
        resultadoEsperado.setTotal_factura(200.0f);
        
        try{
            assertEquals(resultadoEsperado, sut.generarFactura(cli));
            EasyMock.verify(mock);
        }
        catch(FacturaException e){
            fail();
        }
    }
    
    @Test
    public void generarFacturaTestCompC2() {
        cli = new Cliente("cliente1",20.0f);
        mock = EasyMock.createMock(Buscador.class);
        EasyMock.expect(mock.elemPendientes(cli)).andStubReturn(0);
        sut.setBuscador(mock);
        EasyMock.replay(mock);
        
        
        String resultadoEsperado = "No hay nada pendiente de facturar";

        
        try{
            sut.generarFactura(cli);
            fail();
        }
        catch(FacturaException e){
            assertEquals(resultadoEsperado,e.getMessage());
            EasyMock.verify(mock);
        }
    }
}
