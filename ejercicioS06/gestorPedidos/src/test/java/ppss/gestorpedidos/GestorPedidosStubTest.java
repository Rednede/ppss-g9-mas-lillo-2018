/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.gestorpedidos;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;
import ppss.gestorpedidos.Cliente;
import ppss.gestorpedidos.Factura;
import ppss.gestorpedidos.FacturaException;
import ppss.gestorpedidos.GestorPedidos;
import ppss.gestorpedidos.interfaces.EstadoEM;
import ppss.gestorpedidos.interfaces.StubTest;

/**
 *
 * @author ppss
 */
@Category (StubTest.class)
public class GestorPedidosStubTest {
    BuscadorStub stub;
    GestorPedidos sut;
    Cliente cli;
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    
    @Before
    public void setUp() {
        stub =  new BuscadorStub();
        sut = new GestorPedidos();
        
    }
    
    @Test
    public void generarFacturaC1() {
        stub.setElem(10);
        sut.setBuscador(stub);
        cli = new Cliente("cliente1",20.0f);
        Factura resultadoEsperado = new Factura("cliente1");
        resultadoEsperado.setTotal_factura(200.0f);
        try{
            assertEquals(resultadoEsperado, sut.generarFactura(cli));

        }catch(FacturaException e){
            fail();
        }
    }
    
    @Test
    @Category(EstadoEM.class)
    public void generarFacturaC2() {
        stub.setElem(0);
        sut.setBuscador(stub);
        cli = new Cliente("cliente1",20.0f);
        String resultadoEsperado = "No hay nada pendiente de facturar";
        
        try{
            sut.generarFactura(cli);
            fail();

        }catch(FacturaException e){
            assertEquals(resultadoEsperado, e.getMessage());
        }
    }
}
