/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.gestorpedidos;

import org.easymock.EasyMock;
import static org.easymock.EasyMock.anyObject;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;
import ppss.gestorpedidos.Buscador;
import ppss.gestorpedidos.Cliente;
import ppss.gestorpedidos.Factura;
import ppss.gestorpedidos.FacturaException;
import ppss.gestorpedidos.GestorPedidos;
import ppss.gestorpedidos.interfaces.EstadoEM;

/**
 *
 * @author ppss
 */
@Category (EstadoEM.class)
public class GestorPedidoEstEMTest {
    
    GestorPedidos sut;
    Buscador stub;
    Cliente cli;
    @Before
    public void setUp() {
        sut = new GestorPedidos();
        
    }
    
    @Test
    public void generarFacturaTestC1() {
        stub = EasyMock.createNiceMock(Buscador.class);
        EasyMock.expect(stub.elemPendientes(anyObject())).andStubReturn(10);
        sut.setBuscador(stub);
        EasyMock.replay(stub);
        
        cli = new Cliente("cliente1",20.0f);
        Factura resultadoEsperado = new Factura("cliente1");
        resultadoEsperado.setTotal_factura(200.0f);
        
        try{
            assertEquals(resultadoEsperado, sut.generarFactura(cli));
        }
        catch(FacturaException e){
            fail();
        }
    }
    
    @Test
    public void generarFacturaTestC2() {
        stub = EasyMock.createNiceMock(Buscador.class);
        EasyMock.expect(stub.elemPendientes(anyObject())).andStubReturn(0);
        sut.setBuscador(stub);
        EasyMock.replay(stub);
        
        cli = new Cliente("cliente1",20.0f);
        String resultadoEsperado = "No hay nada pendiente de facturar";

        
        try{
            sut.generarFactura(cli);
            fail();
        }
        catch(FacturaException e){
            assertEquals(resultadoEsperado,e.getMessage());
        }
    }
}
