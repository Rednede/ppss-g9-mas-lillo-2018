/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.gestorpedidos;

/**
 *
 * @author ppss
 */
public class Cliente {
    private String idCliente;
    private float precioCliente;
    
    public Cliente(String idCliente, float precioC){
        this.idCliente = idCliente;
        this.precioCliente = precioC;
    }

    public String getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(String idCliente) {
        this.idCliente = idCliente;
    }

    public float getPrecioCliente() {
        return precioCliente;
    }

    public void setPrecioCliente(float precioCliente) {
        this.precioCliente = precioCliente;
    }
    
    
}
