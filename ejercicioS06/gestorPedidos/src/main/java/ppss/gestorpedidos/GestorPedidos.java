/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.gestorpedidos;

/**
 *
 * @author ppss
 */
public class GestorPedidos {
    private Buscador buscarDatos = null;
    public Buscador getBuscador(){
        if(buscarDatos == null){
            Buscador busca = new Buscador();
            return busca;
        }else{
            return buscarDatos;
        }
        
    }
    public void setBuscador(Buscador b){
        buscarDatos = b;
    }
    
    public Factura generarFactura(Cliente cli) throws FacturaException{
        Factura factura = new Factura(cli.getIdCliente());
        buscarDatos = getBuscador();
        
        int numElems = buscarDatos.elemPendientes(cli);
        if(numElems > 0){
            //Código para generar la factura
            factura.setIdCliente(cli.getIdCliente());
            float total = cli.getPrecioCliente()*numElems;
            factura.setTotal_factura(total);
        }else{
            throw new FacturaException("No hay nada pendiente de facturar");
        }
        return factura;
    }
    
}
