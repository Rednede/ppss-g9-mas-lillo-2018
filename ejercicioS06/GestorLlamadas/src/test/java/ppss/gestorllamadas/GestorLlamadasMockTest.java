/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.gestorllamadas;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class GestorLlamadasMockTest {
    
    public GestorLlamadasMockTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        sut = new GestorLlamadas();
    }
    
    @After
    public void tearDown() {
    }
    
    GestorLlamadas sut;
    ServicioHorario mock;
    Double expected;
    Double real;
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void CalculaConsumoMockC1() {
        //minutos 10 hora 15 resultado 208
        mock = EasyMock.createMock(ServicioHorario.class);
        EasyMock.expect(mock.getHoraActual()).andReturn(15);
        sut.setReloj(mock);
        EasyMock.replay(mock);
        expected = 208.0;
        real = sut.calculaConsumo(10);
        assertEquals(expected,real,0.005);
        EasyMock.verify(mock);
        
        
        
        
    }
}
