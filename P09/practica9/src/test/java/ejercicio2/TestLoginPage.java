/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2;

import ejercicio1.*;
import java.util.concurrent.TimeUnit;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author ppss
 */
public class TestLoginPage {
    WebDriver driver;
    LoginPage poLogin;
    ManagerPage poManagerPage;
    NewCustomerPage poNewCustomerPage;
    DeleteCustomerPage poDeleteCustomerPage;
    String user;
    @Before
    public void setup(){
        
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
        poLogin = PageFactory.initElements(driver, LoginPage.class);
    }
    /*@Test
    public void test_Login_Correct(){
        String w1 = driver.getWindowHandle();
        
        String loginPageTitle= poLogin.getPageTitle();
        Assert.assertTrue(loginPageTitle.toLowerCase().contains("guru99 bank"));
        
        poManagerPage = poLogin.login("mngr136927","tYbegYd");
        
        Assert.assertTrue(poManagerPage.getHomePageDashboardUserName().toLowerCase().contains("manger id "));
        
        poManagerPage.logout();
        Alert alert = driver.switchTo().alert();
        String mensaje = alert.getText();
        Assert.assertEquals("You Have Succesfully Logged Out!!",mensaje);
        
        alert.accept();
        driver.switchTo().window(w1);
        driver.close();
    }*/
    /*
     @Test
    public void test_New_Customer(){
        String w1 = driver.getWindowHandle();
        
        String loginPageTitle= poLogin.getPageTitle();
        Assert.assertTrue(loginPageTitle.toLowerCase().contains("guru99 bank"));
        
        poManagerPage = poLogin.login("mngr136927","tYbegYd");
        
        Assert.assertTrue(poManagerPage.getHomePageDashboardUserName().toLowerCase().contains("manger id "));
        
        //vamos a introducir el nuevo Customer
        poNewCustomerPage = poManagerPage.newCustomer();
        poNewCustomerPage.setName("Lour");
        poNewCustomerPage.setM();
        poNewCustomerPage.setDate("1204-06-08");
        poNewCustomerPage.setAddress("mi casa");
        poNewCustomerPage.setCity("Montpellier");
        poNewCustomerPage.setState("France");
        poNewCustomerPage.setPin("123456");
        poNewCustomerPage.setPhone("123654987");
        poNewCustomerPage.setEmail("sedruolh@gmail.com");
        poNewCustomerPage.setPassword("culo");
        poNewCustomerPage.Submit();
        user = poManagerPage.getCustomerId();
        Assert.assertEquals( "Customer Registered Successfully!!!", poManagerPage.customerRegistered());
        
        poManagerPage.logout();
        Alert alert = driver.switchTo().alert();
        String mensaje = alert.getText();
        Assert.assertEquals("You Have Succesfully Logged Out!!",mensaje);
        
        alert.accept();
        //driver.switchTo().window(w1);
        driver.close();
    }
    
    
     @Test
    public void test_Delete_Customer(){
        String w1 = driver.getWindowHandle();
        
        String loginPageTitle= poLogin.getPageTitle();
        Assert.assertTrue(loginPageTitle.toLowerCase().contains("guru99 bank"));
        
        poManagerPage = poLogin.login("mngr136927","tYbegYd");
        
        Assert.assertTrue(poManagerPage.getHomePageDashboardUserName().toLowerCase().contains("manger id "));
        
        //vamos a introducir el nuevo Customer
        poNewCustomerPage = poManagerPage.newCustomer();
        poNewCustomerPage.setName("Lour");
        poNewCustomerPage.setM();
        poNewCustomerPage.setDate("1204-06-08");
        poNewCustomerPage.setAddress("mi casa");
        poNewCustomerPage.setCity("Montpellier");
        poNewCustomerPage.setState("France");
        poNewCustomerPage.setPin("123456");
        poNewCustomerPage.setPhone("123654987");
        poNewCustomerPage.setEmail("sedruolh9@gmail.com");
        poNewCustomerPage.setPassword("culo");
        poNewCustomerPage.Submit();
        user = poManagerPage.getCustomerId();
        Assert.assertEquals( "Customer Registered Successfully!!!", poManagerPage.customerRegistered());
        //Borramos el usuario
        poDeleteCustomerPage = poManagerPage.deleteCustomer();
        poDeleteCustomerPage.delete(user);
        //Gestionamos la alerta
        Alert alerta = driver.switchTo().alert();
        String aceptar = alerta.getText();
        Assert.assertEquals("Do you really want to delete this Customer?", aceptar);
        alerta.accept();
        //confirmamos 
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.alertIsPresent());
        
        Alert alert2 = driver.switchTo().alert();
        String mensaje2 = alert2.getText();
        Assert.assertEquals("Customer deleted Successfully",mensaje2);
        
        alert2.accept();
        
        
        
        poManagerPage.logout();
        Alert alert = driver.switchTo().alert();
        String mensaje = alert.getText();
        Assert.assertEquals("You Have Succesfully Logged Out!!",mensaje);
        
        alert.accept();
        //driver.switchTo().window(w1);
        driver.close();
    }
    
    
    @Test
    public void test_Login_Incorrect(){
        String w1 = driver.getWindowHandle();
        
        String loginPageTitle = poLogin.getPageTitle();
        Assert.assertTrue(loginPageTitle.toLowerCase().contains("guru99 bank"));
        poLogin.login("false","false");
        poManagerPage = new ManagerPage(driver);
        
        Alert alert = driver.switchTo().alert();
        String mensaje = alert.getText();
        Assert.assertEquals("User or Password is not valid",mensaje);
        
        alert.accept();
        driver.switchTo().window(w1);
        driver.close();
        
    }*/
     @Test
    public void test_Repe_Customer(){
        String w1 = driver.getWindowHandle();
        
        String loginPageTitle= poLogin.getPageTitle();
        Assert.assertTrue(loginPageTitle.toLowerCase().contains("guru99 bank"));
        
        poManagerPage = poLogin.login("mngr136927","tYbegYd");
        
        Assert.assertTrue(poManagerPage.getHomePageDashboardUserName().toLowerCase().contains("manger id "));
        
        //vamos a introducir el nuevo Customer
        poNewCustomerPage = poManagerPage.newCustomer();
        poNewCustomerPage.setName("Lourd");
        poNewCustomerPage.setM();
        poNewCustomerPage.setDate("1204-06-08");
        poNewCustomerPage.setAddress("mi casa");
        poNewCustomerPage.setCity("Montpellier");
        poNewCustomerPage.setState("France");
        poNewCustomerPage.setPin("123456");
        poNewCustomerPage.setPhone("123654987");
        poNewCustomerPage.setEmail("ggsedruolh5@gmail.com");
        poNewCustomerPage.setPassword("culo");
        poNewCustomerPage.Submit();
        user = poManagerPage.getCustomerId();
        Assert.assertEquals( "Customer Registered Successfully!!!", poManagerPage.customerRegistered());
        //lo volvemos a introducir
        poNewCustomerPage = poManagerPage.newCustomer();
        poNewCustomerPage.setName("Lourd");
        poNewCustomerPage.setM();
        poNewCustomerPage.setDate("1204-06-08");
        poNewCustomerPage.setAddress("mi casa");
        poNewCustomerPage.setCity("Montpellier");
        poNewCustomerPage.setState("France");
        poNewCustomerPage.setPin("123456");
        poNewCustomerPage.setPhone("123654987");
        poNewCustomerPage.setEmail("ggsedruolh5@gmail.com");
        poNewCustomerPage.setPassword("culo");
        poNewCustomerPage.Submit();
        Alert alerta = driver.switchTo().alert();
        String repetido = alerta.getText();
        Assert.assertEquals("Email Address Already Exist !!",repetido);
        
        alerta.accept();
        
        
        
        poManagerPage.logout();
        Alert alert = driver.switchTo().alert();
        String mensaje = alert.getText();
        Assert.assertEquals("You Have Succesfully Logged Out!!",mensaje);
        
        alert.accept();
        //driver.switchTo().window(w1);
        driver.close();
    }

    
    
    
}
