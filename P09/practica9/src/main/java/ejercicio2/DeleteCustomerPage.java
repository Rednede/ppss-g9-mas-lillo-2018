/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 *
 * @author ppss
 */
public class DeleteCustomerPage {
    WebDriver driver;
    @FindBy(name="cusid") WebElement cid;
    @FindBy(name="AccSubmit") WebElement submit;
    
    
    public DeleteCustomerPage(WebDriver driver){
        this.driver = driver;
    }
    public void delete(String id){
        cid.sendKeys(id);
        submit.click();
    }
}
