/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
/**
 *
 * @author ppss
 */
public class ManagerPage {
    WebDriver driver;
    @FindBy(xpath = "//table//tr[@class='heading3']") WebElement homePageUserName;
    @FindBy(linkText="New Customer") WebElement newCustomer;
    @FindBy(linkText="Delete Customer") WebElement deleteCustomer;
    @FindBy(linkText = "Log out") WebElement logOut;
    //@FindBy(partinkText = "here") WebElement here;
    
    public ManagerPage(WebDriver driver){
        this.driver =driver;

    }
    
    public String getHomePageDashboardUserName(){
        return homePageUserName.getText();
    }
    
    public NewCustomerPage newCustomer(){
        newCustomer.click();
        NewCustomerPage np = PageFactory.initElements(driver, NewCustomerPage.class);
        return np;
    }
    
    public DeleteCustomerPage deleteCustomer(){
        deleteCustomer.click();
        DeleteCustomerPage dp = PageFactory.initElements(driver, DeleteCustomerPage.class);
        return dp;
    }
    
    public void logout(){
        logOut.click();
    }
    public String getCustomerId(){
        return driver.findElement(By.xpath("//html/body/table/tbody/tr/td/table/tbody/tr[4]/td[2]")).getText();
    }
    public String customerRegistered(){
    
        return driver.findElement(By.xpath("//html/body/table/tbody/tr/td/table/tbody/tr[1]/td/p")).getText();
    }
    
    
}
