/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 *
 * @author ppss
 */
public class NewCustomerPage {
    WebDriver driver;
    @FindBy(xpath = "//html/body/table/tbody/tr/td/table/tbody/tr[1]/td/p")
        WebElement pageTitle;
    @FindBy(name = "name") WebElement name;
    @FindBy(xpath = "//html/body/table/tbody/tr/td/table/tbody/tr[5]/td[2]/input[1]") WebElement m;
    @FindBy(xpath = "//html/body/table/tbody/tr/td/table/tbody/tr[5]/td[2]/input[2]") WebElement f;
    @FindBy(name = "dob")WebElement date;
    @FindBy(name = "addr") WebElement address;
    @FindBy(name = "city") WebElement city;
    @FindBy(name = "state") WebElement state;
    @FindBy(name = "pinno") WebElement pin;
    @FindBy(name = "telephoneno") WebElement phone;
    @FindBy(name = "emailid") WebElement email;
    @FindBy(name = "password") WebElement password;
    @FindBy(name = "sub") WebElement submit;
    
    public NewCustomerPage(WebDriver driver){
        this.driver = driver;
    }

    public void setName(String n) {
        this.name.sendKeys(n);
    }

    public void setM() {
        this.m.click();
    }

    public void setF() {
        this.f.click();
    }

    public void setDate(String d) {
        this.date.click();
      
        date.sendKeys(d);
    }

    public void setAddress(String a) {
        this.address.sendKeys(a);
    }

    public void setCity(String c) {
        this.city.sendKeys(c); 
    }

    public void setState(String s) {
        this.state.sendKeys(s);
    }

    public void setPin(String p) {
        this.pin.sendKeys(p); 
    }

    public void setPhone(String p) {
        this.phone.sendKeys(p); 
    }

    public void setEmail(String e) {
        this.email.sendKeys(e); 
    }

    public void setPassword(String p) {
        this.password.sendKeys(p); 
    }

    public void Submit() {
        this.submit.click(); 
    }
    
    
    
    
    public String getTitle(){
        return pageTitle.getText();
    }
}
