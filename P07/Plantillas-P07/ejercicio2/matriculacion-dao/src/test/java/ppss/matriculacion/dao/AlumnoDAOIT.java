/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.matriculacion.dao;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.log4j.BasicConfigurator;
import org.dbunit.Assertion;
import org.dbunit.IDatabaseTester;
import org.dbunit.JdbcDatabaseTester;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.ext.mysql.MySqlDataTypeFactory;
import org.dbunit.util.fileloader.DataFileLoader;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;
import static org.junit.Assert.fail;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ppss.matriculacion.to.AlumnoTO;
/**
 *
 * @author ppss
 */
public class AlumnoDAOIT {
    
    private IAlumnoDAO sut;
    private static final String TABLE_ALUMNO = "alumnos";
    private IDatabaseTester databaseTester;
    
    @BeforeClass
    public static void only_once() {
        BasicConfigurator.configure();
    }
    
    @Before
    public void setUp() throws Exception{
        databaseTester = new JdbcDatabaseTester("com.mysql.jdbc.Driver","jdbc:mysql://localhost:3306/matriculacion?useSSL=false","root","ppss");
    
        DataFileLoader loader = new FlatXmlDataFileLoader();
        IDataSet startingDataSet = loader.load("/tabla2.xml");
    
        databaseTester.setDataSet(startingDataSet);
        databaseTester.onSetup();
        
        sut = new FactoriaDAO().getAlumnoDAO();
    }
    
    @Test
    public void AlumnoDAOTestA1() throws Exception{
    
        AlumnoTO alumno = new AlumnoTO();
        alumno.setNif("33333333C");
        alumno.setNombre("Elena Aguirre Juarez");
        alumno.setDireccion("");
        alumno.setEmail("");
        Calendar fechaNac = Calendar.getInstance();
        fechaNac.set(Calendar.HOUR, 0);
        fechaNac.set(Calendar.MINUTE, 0);
        fechaNac.set(Calendar.SECOND, 0);
        fechaNac.set(Calendar.MILLISECOND, 0);
        fechaNac.set(Calendar.YEAR, 1985);
        fechaNac.set(Calendar.MONTH, 1); 
        fechaNac.set(Calendar.DATE, 22);
        alumno.setFechaNacimiento(fechaNac.getTime());
        
        //Ejercitamos el SUT
        try{
            sut.addAlumno(alumno);
        }catch(DAOException ex){
            Logger.getLogger(AlumnoDAOIT.class.getName()).log(Level.SEVERE, null, ex);
        fail("Se ha lanzado una excepcion no controlada");    
        }
        
        IDatabaseConnection connection = databaseTester.getConnection();
        DatabaseConfig dbconfig = connection.getConfig();
        dbconfig.setProperty("http://www.dbunit.org/properties/datatypeFactory",new MySqlDataTypeFactory() );
        
        //obtenemos los datos de la tablaalumno de la bd
        IDataSet actualDataSet = connection.createDataSet();
        ITable actualTable = actualDataSet.getTable(TABLE_ALUMNO);
        
        //obtenemos los datos esperados del dataset en xml
        DataFileLoader loader = new FlatXmlDataFileLoader();
        IDataSet expectedDataSet = loader.load("/tabla3.xml");
        ITable expectedTable = expectedDataSet.getTable(TABLE_ALUMNO);
        
        Assertion.assertEquals(expectedTable, actualTable);
    }
    @Test
    public void AlumnoDAOTestA2(){
        AlumnoTO alumno = new AlumnoTO();
        alumno.setNif("11111111A");
        alumno.setNombre("Alfonso Ramirez Ruiz");
        alumno.setDireccion("");
        alumno.setEmail("");
        Calendar fechaNac = Calendar.getInstance();
        fechaNac.set(Calendar.HOUR, 0);
        fechaNac.set(Calendar.MINUTE, 0);
        fechaNac.set(Calendar.SECOND, 0);
        fechaNac.set(Calendar.MILLISECOND, 0);
        fechaNac.set(Calendar.YEAR, 1982);
        fechaNac.set(Calendar.MONTH, 1); 
        fechaNac.set(Calendar.DATE, 22);
        alumno.setFechaNacimiento(fechaNac.getTime());
        try{
            sut.addAlumno(alumno);
            fail("Test A2 no ha lanzado la DAOException");
        }catch(DAOException ex){
        
        }
        
    }
    
    @Test
    public void AlumnoDAOTestA3(){
        AlumnoTO alumno = new AlumnoTO();
        alumno.setNif("44444444D");
        alumno.setNombre(null);
        alumno.setDireccion("");
        alumno.setEmail("");
        Calendar fechaNac = Calendar.getInstance();
        fechaNac.set(Calendar.HOUR, 0);
        fechaNac.set(Calendar.MINUTE, 0);
        fechaNac.set(Calendar.SECOND, 0);
        fechaNac.set(Calendar.MILLISECOND, 0);
        fechaNac.set(Calendar.YEAR, 1982);
        fechaNac.set(Calendar.MONTH, 1); 
        fechaNac.set(Calendar.DATE, 22);
        alumno.setFechaNacimiento(fechaNac.getTime());
        
        try{
            sut.addAlumno(alumno);
            fail("test A· no ha lanzado DAOException");
        }catch(DAOException e){}
        
    }
    
    
    @Test
    public void AlumnoDAOTestA4(){
        AlumnoTO alumno = null;
        
        try{
            sut.addAlumno(alumno);
            fail("el test A4 no ha lanzado la excepcion DAOException");
        }catch(DAOException e){}
    
    }
    
    @Test
    public void AlumnoDAOTestA5(){
        AlumnoTO alumno = new AlumnoTO();
        alumno.setNif(null);
        alumno.setNombre("Pedro Garcia Lopez");
        alumno.setDireccion("");
        alumno.setEmail("");
        Calendar fechaNac = Calendar.getInstance();
        fechaNac.set(Calendar.HOUR, 0);
        fechaNac.set(Calendar.MINUTE, 0);
        fechaNac.set(Calendar.SECOND, 0);
        fechaNac.set(Calendar.MILLISECOND, 0);
        fechaNac.set(Calendar.YEAR, 1982);
        fechaNac.set(Calendar.MONTH, 1); 
        fechaNac.set(Calendar.DATE, 22);
        alumno.setFechaNacimiento(fechaNac.getTime());
        
        try{
            sut.addAlumno(alumno);
            fail("El test A5 no ha lanzado DAOException");
        }catch(DAOException e){}
    }
    
    @Test
    public void AlumnoDAOTestB1() throws Exception{
        String nif = "11111111A";
        try{
            sut.delAlumno(nif);
        }catch(DAOException e){
            fail("Test B1 ha lanzado una excepcion DAIException");
        }
        //Recuperamos los datos de la bd
        IDatabaseConnection connection = databaseTester.getConnection();
        DatabaseConfig config = connection.getConfig();
        config.setProperty("http://www.dbunit.org/properties/datatypeFactory", new MySqlDataTypeFactory());
        
        //obtenemos los datos esperados
        IDataSet actualDataSet = connection.createDataSet();
        ITable actualTable = actualDataSet.getTable(TABLE_ALUMNO);
        
        //obtenemos los datos reales
        FlatXmlDataFileLoader loader = new FlatXmlDataFileLoader();
        IDataSet expectedDataSet = loader.load("/tabla4.xml");
        ITable expectedTable = expectedDataSet.getTable(TABLE_ALUMNO);
        
        //comprobamos los resultados
        Assertion.assertEquals(expectedTable, actualTable);
    }
    @Test
    public void AlumnoDAOTestB2(){
    
        String nif = "33333333C";
        try{
            sut.delAlumno(nif);
            fail("No se ha lanzado la excepcion DAOException en el test B2");
        }catch(DAOException e){}
    }
    
    
    
    
}
