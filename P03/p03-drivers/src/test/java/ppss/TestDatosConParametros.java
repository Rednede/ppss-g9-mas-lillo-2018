/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.Test;
import java.util.Collection;
import org.junit.Assert;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
@RunWith(Parameterized.class)
public class TestDatosConParametros {
    
    @Parameterized.Parameters(name = 
            "Caso C{index}: Datos {0} = {1}")
    public static Collection<Object[]> data(){
        Tramo t = new Tramo();
        ArrayList<Integer> a = new ArrayList();
        a.add(3);
        a.add(3);
        a.add(3);
        t.setDuracion(2);
        t.setOrigen(0);
        return Arrays.asList(new Object[][]
        {
            {new ArrayList<Integer>(Arrays.asList(3,3,3)),new Tramo(0,2)},
            {new ArrayList<Integer>(Arrays.asList()),new Tramo(0,0)},
            {new ArrayList<Integer>(Arrays.asList(3)),new Tramo(0,0)},
            {new ArrayList<Integer>(Arrays.asList(-1)),new Tramo(0,0)},
            {new ArrayList<Integer>(Arrays.asList(-1,-1,-1,-1)),new Tramo(0,3)},
            {new ArrayList<Integer>(Arrays.asList(120,140,-10,-10,-10)),new Tramo(2,2)}
            
        });
    
    }
    private ArrayList<Integer> lecturas;
    private Tramo resultadoEsperado;
    private Tramo resultadoReal;
    private Datos d = new Datos();
    
    public TestDatosConParametros(ArrayList<Integer> lecturas, Tramo resultadoEsperado) {
        this.lecturas = lecturas;
        this.resultadoEsperado = resultadoEsperado;
        
    }
    
    @Test
    public void testDatosConParametros() {
        Assert.assertEquals(resultadoEsperado, d.buscarTramoLlanoMasLargo(lecturas));
    }
}
