/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.Categorias;

import ppss.*;
import java.util.ArrayList;
import java.util.Objects;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class TestDatosSinParametros {
    ArrayList<Integer> lecturas = new ArrayList();
    Tramo resultadoEsperado = new Tramo();
    Tramo resultadoReal = new Tramo();
    Datos d = new Datos();
    public TestDatosSinParametros() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
        lecturas.removeAll(lecturas);
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
   @Test
     public void testbuscarTramoLlanoMasLargoC1() {
         lecturas.add(3);
         lecturas.add(3);
         lecturas.add(3);
         resultadoEsperado.setOrigen(0);
         resultadoEsperado.setDuracion(2);
         resultadoReal = d.buscarTramoLlanoMasLargo(lecturas);
         assertEquals(resultadoEsperado.getDuracion(),resultadoReal.getDuracion());
         assertEquals(resultadoEsperado.getOrigen(),resultadoReal.getOrigen());
     }
     
     @Test
     public void testbuscarTramoLlanoMasLargoC2() {
 
         resultadoEsperado.setOrigen(0);
         resultadoEsperado.setDuracion(0);
         resultadoReal = d.buscarTramoLlanoMasLargo(lecturas);
         assertEquals(resultadoEsperado.getDuracion(),resultadoReal.getDuracion());
         assertEquals(resultadoEsperado.getOrigen(),resultadoReal.getOrigen());
     }
     @Test
     public void testbuscarTramoLlanoMasLargoC3() {
 
         lecturas.add(3);
         resultadoEsperado.setOrigen(0);
         resultadoEsperado.setDuracion(0);
         resultadoReal = d.buscarTramoLlanoMasLargo(lecturas);
         //assertEquals(resultadoEsperado.getDuracion(),resultadoReal.getDuracion());
         //assertEquals(resultadoEsperado.getOrigen(),resultadoReal.getOrigen());
         assertEquals(resultadoEsperado,resultadoReal);
     }
     
     @Test
     public void testbuscarTramoLlanoMasLargoC1B() {
 
         lecturas.add(-1);
         resultadoEsperado.setOrigen(0);
         resultadoEsperado.setDuracion(0);
         resultadoReal = d.buscarTramoLlanoMasLargo(lecturas);
         assertEquals(resultadoEsperado.getDuracion(),resultadoReal.getDuracion());
         assertEquals(resultadoEsperado.getOrigen(),resultadoReal.getOrigen());
         //assertEquals(resultadoEsperado,resultadoReal);
     }
     
     @Test
     public void testbuscarTramoLlanoMasLargoC2B() {
 
         lecturas.add(-1);
         lecturas.add(-1);
         lecturas.add(-1);
         lecturas.add(-1);
         resultadoEsperado.setOrigen(0);
         resultadoEsperado.setDuracion(3);
         resultadoReal = d.buscarTramoLlanoMasLargo(lecturas);
         //assertEquals(resultadoEsperado.getDuracion(),resultadoReal.getDuracion());
         //assertEquals(resultadoEsperado.getOrigen(),resultadoReal.getOrigen());
         assertEquals(resultadoEsperado,resultadoReal);
     }
     
     @Test
     public void testbuscarTramoLlanoMasLargoC3B() {
 
         lecturas.add(120);
         lecturas.add(140);
         lecturas.add(-10);
         lecturas.add(-10);
         lecturas.add(-10);
         resultadoEsperado.setOrigen(2);
         resultadoEsperado.setDuracion(2);
         resultadoReal = d.buscarTramoLlanoMasLargo(lecturas);
         //assertEquals(resultadoEsperado.getDuracion(),resultadoReal.getDuracion());
         //assertEquals(resultadoEsperado.getOrigen(),resultadoReal.getOrigen());
         assertEquals(resultadoEsperado,resultadoReal);
     }

     
    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final TestDatosSinParametros other = (TestDatosSinParametros) obj;
        if (!Objects.equals(this.resultadoEsperado, other.resultadoEsperado)) {
            return false;
        }
        if (!Objects.equals(this.resultadoReal, other.resultadoReal)) {
            return false;
        }
        return true;
    }
}
