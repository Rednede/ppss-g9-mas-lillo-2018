/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.Categorias;

import ppss.*;
import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.Test;
import static org.junit.Assert.*;
import java.util.Collection;
import junit.framework.Assert;

/**
 *
 * @author ppss
 */
@RunWith(Parameterized.class)
public class TestMatriculaConParametros {
    
    @Parameterized.Parameters(name = 
            "Caso C{index}: matricula {0},{1},{2} = {3}")
    public static Collection<Object[]> data()
    {
        return Arrays.asList(new Object[][] 
        {
            {24, false, true, 2000.0f},
            {24, false, false, 500.0f},
            {24, true, false, 250.0f},
            {65, false, false, 250.0f},
            {51, false, false, 400.0f}
        });
    }
    private float resultadoEsperado;
    private int edad;
    private boolean familiaNum, repetidor;
    private Matricula m = new Matricula();

    public TestMatriculaConParametros(int edad, boolean familiaNum, boolean repetidor, float resultadoEsperado)
    {
        this.edad = edad;
        this.familiaNum = familiaNum;
        this.repetidor = repetidor;
        this.resultadoEsperado = resultadoEsperado;
    }
    
    @Test
    public void testCalculaTasaMatricula() {
        Assert.assertEquals(resultadoEsperado, m.calculaTasaMatricula(edad, familiaNum, repetidor), 0.008f);
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
