/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.Categorias;

import ppss.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class TestMatriculaSinParametros {
    int edad;
    Boolean familiaNum;
    Boolean repetidor;
    float resultadoEsperado;
    float resultadoReal;
    Matricula m = new Matricula();
    /*public TestMatriculaSinParametros() {
    }*/
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
        
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void testCalculaTasaMatriculaC1() {
        edad = 24;
        familiaNum = false;
        repetidor = true;
        resultadoEsperado = 2000.0f;
        resultadoReal = m.calculaTasaMatricula(edad, familiaNum, repetidor);
        assertEquals(resultadoEsperado, resultadoReal, 0.008f);
    }
    
    @Test
    public void testCalculaTasaMatriculaC2() {
        edad = 24;
        familiaNum = false;
        repetidor = false;
        resultadoEsperado = 500.0f;
        resultadoReal = m.calculaTasaMatricula(edad, familiaNum, repetidor);
        assertEquals(resultadoEsperado, resultadoReal, 0.008f);
    }
    
    @Test
    public void testCalculaTasaMatriculaC3() {
        edad = 24;
        familiaNum = true;
        repetidor = false;
        resultadoEsperado = 250.0f;
        resultadoReal = m.calculaTasaMatricula(edad, familiaNum, repetidor);
        assertEquals(resultadoEsperado, resultadoReal, 0.008f);
    }
    
    @Test
    public void testCalculaTasaMatriculaC4() {
        edad = 65;
        familiaNum = false;
        repetidor = false;
        resultadoEsperado = 250.0f;
        resultadoReal = m.calculaTasaMatricula(edad, familiaNum, repetidor);
        assertEquals(resultadoEsperado, resultadoReal, 0.008f);
    }
    
    @Test
    public void testCalculaTasaMatriculaC5() {
        edad = 51;
        familiaNum = false;
        repetidor = false;
        resultadoEsperado = 400.0f;
        resultadoReal = m.calculaTasaMatricula(edad, familiaNum, repetidor);
        assertEquals(resultadoEsperado, resultadoReal, 0.008f);
    }
}
