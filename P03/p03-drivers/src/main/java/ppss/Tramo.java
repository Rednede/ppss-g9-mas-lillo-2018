/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

/**
 *
 * @author ppss
 */
public class Tramo{
    private int origen;
    private int duracion;
    
    public Tramo(){
        this.origen = 0;
        this.duracion = 0;
    }
    
    public Tramo(int origen, int duracion){
        this.origen = origen;
        this.duracion = duracion;
    }
    public void setOrigen(int origen){
        this.origen = origen;
    }
    public void setDuracion(int duracion){
        this.duracion = duracion;
    }
    public int getOrigen(){
        return this.origen;
    }
    public int getDuracion(){
        return this.duracion;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Tramo other = (Tramo) obj;
        if (this.origen != other.origen) {
            return false;
        }
        if (this.duracion != other.duracion) {
            return false;
        }
        return true;
    }
    
}
