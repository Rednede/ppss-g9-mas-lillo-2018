/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;
import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
/**
 *
 * @author ppss
 */
public class DataArrayTest {
    private int[] coleccion;
    private int numElem, elem;
    int[] resultadoEsperado, resultadoReal;

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
    
    @Test
    public void testAddC1(){
        coleccion = new int[10];
        numElem=0;
        elem = 2;
        DataArray data = new DataArray();
        
        int[] resultadoEsperado = {2,0,0,0,0,0,0,0,0,0};    
        data.add(elem);
        int[] resultadoReal = data.getColeccion();
        assertArrayEquals(resultadoEsperado,resultadoReal);
    }
    
    @Test
    public void testAddC2(){
        coleccion = new int[] {2,5,8,0,0,0,0,0,0,0}; 
        numElem = 3;
        elem = 2;
        DataArray data = new DataArray(coleccion,numElem);
        data.add(elem);
        
        resultadoEsperado = new int[] {2,5,8,2,0,0,0,0,0,0};
        int [] resultadoReal = data.getColeccion();
        assertArrayEquals(resultadoEsperado, resultadoReal);
    }
    
    @Test
    public void testAddC3(){
        coleccion = new int[] {2,2,2,2,2,2,2,2,2,2};
        numElem = 10;
        elem = 4;
        DataArray data = new DataArray(coleccion, numElem);
        data.add(elem);
        resultadoEsperado = new int[] {2,2,2,2,2,2,2,2,2,2};
        int [] resultadoReal = data.getColeccion();
        assertArrayEquals(resultadoEsperado, resultadoReal);
    }
    
    @Test
    public void testDeleteC1(){
        coleccion = new int[] {1,1,2,3,4,5,6,7,8,0};
        numElem = 9;
        elem = 9;
        DataArray data = new DataArray(coleccion, numElem);
        
        resultadoEsperado = new int[] {1,1,2,3,4,5,6,7,8,0};
        int [] resultadoReal = data.delete(elem);
        assertArrayEquals(resultadoEsperado, resultadoReal);
    }
    @Test
    public void testDeleteC2(){
        coleccion = new int[] {1,1,2,3,4,5,6,7,8,0};
        numElem = 9;
        elem = 1;
        DataArray data = new DataArray(coleccion, numElem);
        
        resultadoEsperado = new int[] {1,2,3,4,5,6,7,8,0,0};
        int [] resultadoReal = data.delete(elem);
        assertArrayEquals(resultadoEsperado, resultadoReal);
    }
    
    @Test
    public void testDeleteC3(){
        coleccion = new int[] {1,1,3,3,4,5,6,7,8,8};
        numElem = 10;
        elem = 1;
        DataArray data = new DataArray(coleccion, numElem);
        
        resultadoEsperado= new int[] {1,3,3,4,5,6,7,8,8,0};
        int [] resultadoReal = data.delete(elem);
        assertArrayEquals(resultadoEsperado, resultadoReal);
        
    }

    /**
     * Test of size method, of class DataArray.
     */
    /*
    @Test
    public void testGetColeccion() {
        System.out.println("getColeccion");
        DataArray instance = new DataArray();
        int[] expResult = null;
        int[] result = instance.getColeccion();
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }
*/
}
