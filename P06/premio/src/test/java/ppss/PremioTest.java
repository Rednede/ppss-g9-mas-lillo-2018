/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;



import org.easymock.EasyMock;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import ppss.exceptions.ClienteWebServiceException;

/**
 *
 * @author ppss
 */
public class PremioTest {
    private Premio sut;
    private ClienteWebService mock;
    private String expected;
    private String real;
    
    @Before
    public void setUp() {
        sut = new Premio();
    }
    @Test
    public void compruebaPremioC1(){
        sut = EasyMock.partialMockBuilder(Premio.class).addMockedMethod("generaNumero").createMock();
        EasyMock.expect(sut.generaNumero()).andReturn(0.07f);
        EasyMock.replay(sut);
        mock = EasyMock.createMock(ClienteWebService.class);
        try{
            EasyMock.expect(mock.obtenerPremio()).andReturn("entrada final Champions");
        }catch(ClienteWebServiceException e){
            fail("Error en la preparación del mock");
        } 
        sut.setCliente(mock);
        EasyMock.replay(mock);
        
        expected = "Premiado con entrada final Champions";
        real = sut.compruebaPremio();
        assertEquals(expected, real);
        
        EasyMock.verify(sut);
        EasyMock.verify(mock); 
    }
    
    @Test
    public void compruebaPremioC2(){
        sut = EasyMock.partialMockBuilder(Premio.class).addMockedMethod("generaNumero").createMock();
        EasyMock.expect(sut.generaNumero()).andReturn(0.03f);
        EasyMock.replay(sut);
        mock = EasyMock.createMock(ClienteWebService.class);
        try{
            EasyMock.expect(mock.obtenerPremio()).andThrow(new ClienteWebServiceException() );
            
        }catch(ClienteWebServiceException e){
            fail();
        } 
        sut.setCliente(mock);
        EasyMock.replay(mock);
        
        expected = "No se ha podido obtener el premio";
        real = sut.compruebaPremio();
        assertEquals(expected, real);
        
        EasyMock.verify(sut);
        EasyMock.verify(mock); 
    }
}
