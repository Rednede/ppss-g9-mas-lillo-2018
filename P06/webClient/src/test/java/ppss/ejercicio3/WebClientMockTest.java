/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class WebClientMockTest {
    WebClient sut ;
    URL url;
    HttpURLConnection mock;
    String expected;
    String real;
    @Before
    public void setUp() {

        sut = EasyMock.partialMockBuilder(WebClient.class).addMockedMethods("createHttpURLConnection").createMock();
        mock = EasyMock.createMock(HttpURLConnection.class);
        
    }
    @Test
    public void getContentTestC1(){
        String direccionHttp = "http://www.google.es";
        
        try {
            url = new URL( direccionHttp);
        } catch (MalformedURLException ex) {
            Logger.getLogger(WebClientMockTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        try {
            EasyMock.expect(sut.createHttpURLConnection(url)).andReturn(mock);
        } catch (IOException ex) {
            Logger.getLogger(WebClientMockTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        try {
            EasyMock.expect(mock.getInputStream()).andReturn(new ByteArrayInputStream("Se puede abrir la conexión".getBytes()));
        } catch (IOException ex) {
            Logger.getLogger(WebClientMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }
        EasyMock.replay(mock);
        EasyMock.replay(sut);
        
        
        expected = "Se puede abrir la conexión";
        real = sut.getContent(url);
        assertEquals(expected, real);
        EasyMock.verify(mock);
        EasyMock.verify(sut);
        
        
    }
    
    @Test
    public void getContentTestC2(){
        String direccionHttp = "http://www.google.es";
        
        try {
            url = new URL( direccionHttp);
        } catch (MalformedURLException ex) {
            Logger.getLogger(WebClientMockTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            EasyMock.expect(sut.createHttpURLConnection(url)).andReturn(mock);
        } catch (IOException ex) {
            Logger.getLogger(WebClientMockTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        
        try {
            EasyMock.expect(mock.getInputStream()).andThrow(new IOException());
        } catch (IOException ex) {
            Logger.getLogger(WebClientMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }
        EasyMock.replay(mock);
        EasyMock.replay(sut);
        String a = null;
        assertEquals(a, sut.getContent(url));
        EasyMock.verify(mock);
        EasyMock.verify(sut);
        
    }
}
