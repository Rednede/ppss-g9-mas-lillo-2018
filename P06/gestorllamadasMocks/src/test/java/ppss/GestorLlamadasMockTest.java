/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

import org.easymock.EasyMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class GestorLlamadasMockTest {
    GestorLlamadas sut;
    Calendario mock;
    double expected;
    double real;
    int minutos;
    @Before
    public void setUp() {
        sut = new GestorLlamadas();
        mock = new Calendario();
    }
    @Test
    public void calculaConsumoTestC1() {
        minutos = 22;
        mock = EasyMock.createMock(Calendario.class);
        EasyMock.expect(mock.getHoraActual()).andReturn(10);
        sut.setC(mock);
        EasyMock.replay(mock);
        
        expected = 457.6;
        real = sut.calculaConsumo(minutos);
        EasyMock.verify(mock);
    }
    
    @Test
    public void CalculaConsumoTestC2(){
    minutos = 13;
    mock = EasyMock.createMock(Calendario.class);
    EasyMock.expect(mock.getHoraActual()).andReturn(21);
    sut.setC(mock);
    EasyMock.replay(mock);
    
    expected = 136.5;
    real = sut.calculaConsumo(minutos);
    EasyMock.verify(mock);
    }
}
