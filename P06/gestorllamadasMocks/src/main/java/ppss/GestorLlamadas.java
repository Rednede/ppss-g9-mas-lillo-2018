/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;

/**
 *
 * @author ppss
 */
public class GestorLlamadas {
    static double TARIFA_NOCTURNA = 10.5;
    static double TARIFA_DIURNA = 20.8;
    private Calendario c = null;
    public Calendario getCalendario(){
        if (c == null){
            c = new Calendario();
            return c;
        }else{
            return c;
        }
    }
    public double calculaConsumo(int minutos){
        c = getCalendario();
        int hora = c.getHoraActual();
        if(hora < 8 || hora > 20){
            return minutos * TARIFA_NOCTURNA;
        }else{
            return minutos * TARIFA_DIURNA;
        }
    }

    public static double getTARIFA_NOCTURNA() {
        return TARIFA_NOCTURNA;
    }

    public static void setTARIFA_NOCTURNA(double TARIFA_NOCTURNA) {
        GestorLlamadas.TARIFA_NOCTURNA = TARIFA_NOCTURNA;
    }

    public static double getTARIFA_DIURNA() {
        return TARIFA_DIURNA;
    }

    public static void setTARIFA_DIURNA(double TARIFA_DIURNA) {
        GestorLlamadas.TARIFA_DIURNA = TARIFA_DIURNA;
    }

    public Calendario getC() {
        return c;
    }

    public void setC(Calendario c) {
        this.c = c;
    }
    
}
