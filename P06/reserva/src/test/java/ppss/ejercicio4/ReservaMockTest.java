/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio4;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.easymock.EasyMock;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import ppss.ejercicio4.excepciones.IsbnInvalidoException;
import ppss.ejercicio4.excepciones.JDBCException;
import ppss.ejercicio4.excepciones.ReservaException;
import ppss.ejercicio4.excepciones.SocioInvalidoException;

/**
 *
 * @author ppss
 */
public class ReservaMockTest {
    Reserva sut;
    FactoriaBOs mockF;
    IOperacionBO mockIO;
    String login, password, socio, expected, real;
    String [] isbns;

    @Before
    public void setUp() {
        sut = EasyMock.partialMockBuilder(Reserva.class)
                .addMockedMethods("compruebaPermisos","getFactoriaBOs")
                .createMock();
        
        mockF = EasyMock.createMock(FactoriaBOs.class);
        mockIO = EasyMock.createMock(IOperacionBO.class);
    }
    
    @Test
    public void realizaReservaMockTestC1(){
        login = "xxxx";
        password = "xxxx";
        socio = "Pepe";
        isbns = new String[]{"22222"};
        
        EasyMock.expect((sut.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO))).andReturn(false);
        EasyMock.replay(sut);
        EasyMock.replay(mockF);
        EasyMock.replay(mockIO);
        
        expected = "ERROR de permisos; ";
        try{
            sut.realizaReserva(login, password, socio, isbns);
            fail();
        }catch(ReservaException e){
            assertEquals(expected, e.getMessage());
        }
        EasyMock.verify(sut);
        EasyMock.verify(mockF);
        EasyMock.verify(mockIO);
    }
    
    @Test
    public void realizaReservaMockTestC2(){
        login = "ppss";
        password = "ppss";
        socio = "Pepe";
        isbns = new String[]{"22222", "33333"};
        
        EasyMock.expect((sut.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO))).andReturn(true);
        EasyMock.expect(sut.getFactoriaBOs()).andReturn(mockF);
        EasyMock.expect(mockF.getOperacionBO()).andReturn(mockIO);
        try {
            mockIO.operacionReserva(socio, "22222");
            mockIO.operacionReserva(socio, "33333");
        } catch (IsbnInvalidoException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        } catch (JDBCException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        } catch (SocioInvalidoException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }
        
        EasyMock.replay(sut);
        EasyMock.replay(mockF);
        EasyMock.replay(mockIO);

        try{
            sut.realizaReserva(login, password, socio, isbns);
            
        }catch(ReservaException e){
            fail();
        }       
        EasyMock.verify(sut);
        EasyMock.verify(mockF);
        EasyMock.verify(mockIO);
    }
    
    @Test
    public void realizaReservaMockTestC3(){
        login = "ppss";
        password = "ppss";
        socio = "Pepe";
        isbns = new String[]{"11111"};
        
        EasyMock.expect((sut.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO))).andReturn(true);
        EasyMock.expect(sut.getFactoriaBOs()).andReturn(mockF);
        EasyMock.expect(mockF.getOperacionBO()).andReturn(mockIO);
        try {
            mockIO.operacionReserva(socio, "11111");
            EasyMock.expectLastCall().andThrow(new IsbnInvalidoException());

        } catch (IsbnInvalidoException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        } catch (JDBCException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        } catch (SocioInvalidoException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }
        
        EasyMock.replay(sut);
        EasyMock.replay(mockF);
        EasyMock.replay(mockIO);
        
        expected = "ISBN invalido:11111; ";
        try{
            sut.realizaReserva(login, password, socio, isbns);
            fail();
        }catch(ReservaException e){
            assertEquals(expected, e.getMessage());
        }       
        EasyMock.verify(sut);
        EasyMock.verify(mockF);
        EasyMock.verify(mockIO);
    }
    @Test
    public void realizaReservaMockTestC4(){
        login = "ppss";
        password = "ppss";
        socio = "Luis";
        isbns = new String[]{"11111"};
        
        EasyMock.expect((sut.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO))).andReturn(true);
        EasyMock.expect(sut.getFactoriaBOs()).andReturn(mockF);
        EasyMock.expect(mockF.getOperacionBO()).andReturn(mockIO);
        try {
            mockIO.operacionReserva(socio, "11111");
            EasyMock.expectLastCall().andThrow(new SocioInvalidoException());

        } catch (IsbnInvalidoException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        } catch (JDBCException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        } catch (SocioInvalidoException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }
        
        EasyMock.replay(sut);
        EasyMock.replay(mockF);
        EasyMock.replay(mockIO);
        
        expected = "SOCIO invalido; ";
        try{
            sut.realizaReserva(login, password, socio, isbns);
            fail();
        }catch(ReservaException e){
            assertEquals(expected, e.getMessage());
        }       
        EasyMock.verify(sut);
        EasyMock.verify(mockF);
        EasyMock.verify(mockIO);
    }
    
    @Test
    public void realizaReservaMockTestC5(){
        login = "ppss";
        password = "ppss";
        socio = "Pepe";
        isbns = new String[]{"11111"};
        
        EasyMock.expect((sut.compruebaPermisos(login, password, Usuario.BIBLIOTECARIO))).andReturn(true);
        EasyMock.expect(sut.getFactoriaBOs()).andReturn(mockF);
        EasyMock.expect(mockF.getOperacionBO()).andReturn(mockIO);
        try {
            mockIO.operacionReserva(socio, "11111");
            EasyMock.expectLastCall().andThrow(new JDBCException());

        } catch (IsbnInvalidoException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        } catch (JDBCException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        } catch (SocioInvalidoException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }
        
        EasyMock.replay(sut);
        EasyMock.replay(mockF);
        EasyMock.replay(mockIO);
        
        expected = "CONEXION invalida; ";
        try{
            sut.realizaReserva(login, password, socio, isbns);
            fail();
        }catch(ReservaException e){
            assertEquals(expected, e.getMessage());
        }       
        EasyMock.verify(sut);
        EasyMock.verify(mockF);
        EasyMock.verify(mockIO);
    }
    
    
}
