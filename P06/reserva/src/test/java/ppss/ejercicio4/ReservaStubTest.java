/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio4;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.easymock.EasyMock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.experimental.categories.Category;
import ppss.ejercicio4.excepciones.IsbnInvalidoException;
import ppss.ejercicio4.excepciones.JDBCException;
import ppss.ejercicio4.excepciones.ReservaException;
import ppss.ejercicio4.excepciones.SocioInvalidoException;

/**
 *
 * @author ppss
 */

@Category (ppss.ejercicio4.interfaces.stubInterface.class)
public class ReservaStubTest {
    
    Reserva sut;
    FactoriaBOs mockF;
    IOperacionBO mockIO;
    String login, password, socio, expected, real;
    String [] isbns;

    @Before
    public void setUp() {
        sut = EasyMock.partialMockBuilder(Reserva.class)
                .addMockedMethods("compruebaPermisos","getFactoriaBOs")
                .createNiceMock();
        
        mockF = EasyMock.createNiceMock(FactoriaBOs.class);
        mockIO = EasyMock.createNiceMock(IOperacionBO.class);
    }
    
    @Test
    public void realizaReservaMockTestC1(){
        login = "xxxx";
        password = "xxxx";
        socio = "Pepe";
        isbns = new String[]{"22222"};
        
        EasyMock.expect(sut.compruebaPermisos(EasyMock.anyString(), EasyMock.anyString(), EasyMock.anyObject())).andStubReturn(false);
        EasyMock.replay(sut);
        EasyMock.replay(mockF);
        EasyMock.replay(mockIO);
        
        expected = "ERROR de permisos; ";
        try{
            sut.realizaReserva(login, password, socio, isbns);
            fail();
        }catch(ReservaException e){
            assertEquals(expected, e.getMessage());
        }

    }
    
    @Test
    public void realizaReservaMockTestC2(){
        login = "ppss";
        password = "ppss";
        socio = "Pepe";
        isbns = new String[]{"22222", "33333"};
        
        EasyMock.expect((sut.compruebaPermisos(EasyMock.anyString(), EasyMock.anyString(), EasyMock.anyObject()))).andStubReturn(true);
        EasyMock.expect(sut.getFactoriaBOs()).andStubReturn(mockF);
        EasyMock.expect(mockF.getOperacionBO()).andStubReturn(mockIO);
        try {
            mockIO.operacionReserva(EasyMock.anyString(), EasyMock.anyString());
            EasyMock.expectLastCall().asStub();
            mockIO.operacionReserva(EasyMock.anyString(), EasyMock.anyString());
            EasyMock.expectLastCall().asStub();
        } catch (IsbnInvalidoException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        } catch (JDBCException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        } catch (SocioInvalidoException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }
        
        EasyMock.replay(sut);
        EasyMock.replay(mockF);
        EasyMock.replay(mockIO);

        try{
            sut.realizaReserva(login, password, socio, isbns);
            
        }catch(ReservaException e){
            fail();
        }       


    }
    
    @Test
    public void realizaReservaMockTestC3(){
        login = "ppss";
        password = "ppss";
        socio = "Pepe";
        isbns = new String[]{"11111"};
        
        EasyMock.expect((sut.compruebaPermisos(EasyMock.anyString(), EasyMock.anyString(), EasyMock.anyObject()))).andStubReturn(true);
        EasyMock.expect(sut.getFactoriaBOs()).andStubReturn(mockF);
        EasyMock.expect(mockF.getOperacionBO()).andStubReturn(mockIO);
        try {
            mockIO.operacionReserva(EasyMock.anyString(), EasyMock.anyString());
            EasyMock.expectLastCall().andStubThrow(new IsbnInvalidoException());

        } catch (IsbnInvalidoException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        } catch (JDBCException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        } catch (SocioInvalidoException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }
        
        EasyMock.replay(sut);
        EasyMock.replay(mockF);
        EasyMock.replay(mockIO);
        
        expected = "ISBN invalido:11111; ";
        try{
            sut.realizaReserva(login, password, socio, isbns);
            fail();
        }catch(ReservaException e){
            assertEquals(expected, e.getMessage());
        }       

    }
    @Test
    public void realizaReservaMockTestC4(){
        login = "ppss";
        password = "ppss";
        socio = "Luis";
        isbns = new String[]{"11111"};
        
        EasyMock.expect((sut.compruebaPermisos(EasyMock.anyString(), EasyMock.anyString(), EasyMock.anyObject()))).andStubReturn(true);
        EasyMock.expect(sut.getFactoriaBOs()).andStubReturn(mockF);
        EasyMock.expect(mockF.getOperacionBO()).andStubReturn(mockIO);
        try {
            mockIO.operacionReserva(EasyMock.anyString(), EasyMock.anyString());
            EasyMock.expectLastCall().andStubThrow(new SocioInvalidoException());

        } catch (IsbnInvalidoException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        } catch (JDBCException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        } catch (SocioInvalidoException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }
        
        EasyMock.replay(sut);
        EasyMock.replay(mockF);
        EasyMock.replay(mockIO);
        
        expected = "SOCIO invalido; ";
        try{
            sut.realizaReserva(login, password, socio, isbns);
            fail();
        }catch(ReservaException e){
            assertEquals(expected, e.getMessage());
        }       

    }
    
    @Test
    public void realizaReservaMockTestC5(){
        login = "ppss";
        password = "ppss";
        socio = "Pepe";
        isbns = new String[]{"11111"};
        
        EasyMock.expect((sut.compruebaPermisos(EasyMock.anyString(), EasyMock.anyString(), EasyMock.anyObject())))
                .andStubReturn(true);
        EasyMock.expect(sut.getFactoriaBOs()).andStubReturn(mockF);
        EasyMock.expect(mockF.getOperacionBO()).andStubReturn(mockIO);
        try {
            mockIO.operacionReserva(EasyMock.anyString(), EasyMock.anyString());
            EasyMock.expectLastCall().andStubThrow(new JDBCException());

        } catch (IsbnInvalidoException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        } catch (JDBCException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        } catch (SocioInvalidoException ex) {
            Logger.getLogger(ReservaMockTest.class.getName()).log(Level.SEVERE, null, ex);
            fail();
        }
        
        EasyMock.replay(sut);
        EasyMock.replay(mockF);
        EasyMock.replay(mockIO);

        expected = "CONEXION invalida; ";
        try{
            sut.realizaReserva(login, password, socio, isbns);
            fail();
        }catch(ReservaException e){
            assertEquals(expected, e.getMessage());
        }       

    }

}
