/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio1;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class TestMultipathExample {
    MultipathExample sut;
    public TestMultipathExample() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        sut = new MultipathExample();
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    @Test
    public void test1() {
    int a = 4;
    int b = 6;
    int c = 0;
    
    int expected = 6;
    
    Assert.assertEquals(expected, sut.multipath(a, b, c) );
            
    }
    @Test
    public void test2() {
    int a = 6;
    int b = 2;
    int c = 0;
    
    int expected = 6;
    
    Assert.assertEquals(expected, sut.multipath(a, b, c) );
            
    }
}
